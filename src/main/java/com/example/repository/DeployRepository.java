package com.example.repository;

import com.example.entity.Deploy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeployRepository extends JpaRepository<Deploy, String> {

    Deploy findOneByUuid(String uuid);

    List<Deploy> findByEnv(String env);

    List<Deploy> findByEnvAndNameLike(String env,String name);

    @Query("select distinct env from Deploy ")
    List<String> findEnvList();
}