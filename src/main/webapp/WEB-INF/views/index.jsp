<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<title>统一日志监控平台</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/materialize/0.97.0/css/materialize.min.css">
<link href="${pageContext.request.contextPath}/resources/css/icon.css" rel="stylesheet">
<script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/materialize/0.97.0/js/materialize.min.js"></script>
<script type="text/javascript">
    function reloadPage(env,name) {
        $.ajax({
            type: "GET",
            url: '${pageContext.request.contextPath}/findList',
            data: "env=" + env+"&name="+name,
            success: function(data) {
                // alert(data);
                $("#content").empty();
                $("#content").append(data);//将返回的数据追加到表格
            }
        });
    };

    $(document).ready(function() {

        var env=$("a.active").attr("data-env");

        reloadPage(env,'');

        $('#java-web-deploy-search').bind('keypress',function(event){
            if(event.keyCode == "13")
            {
                var env=$("a.active").attr("data-env");
               // alert($("a.active").attr("data-env"));
                reloadPage(env,$('#java-web-deploy-search').val());
            }
        });
	});

</script>
</head>
<body style="font-family: 'Roboto', 'Droid Sans Fallback', '微软雅黑'; min-height: 100vh;display: flex;flex-direction: column;">

	<nav style="background-color: #ddd;">
		<div class="nav-wrapper">
			<a href="${pageContext.request.contextPath}/" class="brand-logo center">统一日志监控平台</a>
		</div>
	</nav>

	<div class="container" style="padding-top: 20px; width: 90%;flex: 1 0 auto;">
		<div class="row">
			<div class="col s12">
				<ul class="tabs">
					<c:forEach items="${envList}" var="li">
						<li class="tab col  s6"> <a href="javascript:void(0);" alt="${env}" <c:if test="${env eq li }"> class="active" alt="${env}"</c:if>  data-env="${li}" onclick="reloadPage('${li}',$('#java-web-deploy-search').val())"> ${li} 机器分布 </a></li>
					</c:forEach>
				</ul>
			</div>
		</div>
		<div id="java-web-deploy" class="row">
			<div class="row">
				<div class="input-field col s12 m5 offset-m3">
					<nav style="background-color: #ddd;">
						<div class="nav-wrapper">
							<div class="input-field" style="height: 70%">
								<input id="java-web-deploy-search" type="search" >
								<label for="java-web-deploy-search">
									<i class="material-icons" style="line-height: inherit;">search</i>
								</label>
							</div>
						</div>
					</nav>
				</div>
				<div class="input-field col s12 m4">
					<a class="waves-effect waves-light btn lighten-2" href="${pageContext.request.contextPath}/config/add" style="line-height: 64px; height: 64px;">创建</a>
				</div>
			</div>
			<table class="hoverable">
				<thead>
				<tr>
                    <td>UUID</td>
					<td>项目名称</td>
					<td>服务器 IP</td>
                    <td>日志路径</td>
					<td>删除</td>
					<td>日志</td>
				</tr>
				</thead>
				<tbody id="content">

				</tbody>
			</table>
		</div>
	</div>

	<footer class="page-footer" style="padding-top: 0; margin-top: 40px; background-color: #ddd;">
		<div class="footer-copyright">
			<div class="container">
				Copyright © 2016
			</div>
		</div>
	</footer>

</body>
</html>